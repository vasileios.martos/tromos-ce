// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package main // import "gitlab.com/tromos/tromos-ce/cmd/tromos-daemon"

import (
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

func newDaemonCommands() (*cobra.Command, *cobra.Command, error) {

	var tromosd *peer.Peer

	startCmd := &cobra.Command{
		Use:          "start  [OPTIONS]",
		Short:        "run the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		//DisableFlagsInUseLine: true,
		//Args:                  cli.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {

			instance, err := peer.New()
			if err != nil {
				return err
			}
			tromosd = instance

			if err := instance.RunWebservice(); err != nil {
				return err
			}

			log.User("Daemon succesfully terminated")
			return nil
		},
	}

	// Link the startCommand with the daemon cli
	RootCmd.AddCommand(startCmd)

	stopCmd := &cobra.Command{
		Use:          "stop  [OPTIONS]",
		Short:        "stop the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			if tromosd != nil {
				return tromosd.Shutdown()
			}
			return nil
		},
	}

	// Link the stopCommand with the daemon cli
	//RootCmd.AddCommand(stopCmd)

	return startCmd, stopCmd, nil
}
