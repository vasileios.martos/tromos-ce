// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package hub

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"os"
	"reflect"
	"testing"

	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
)

var pluginList []string
var mani manifest.Manifest

func init() {
	if len(os.Args) != 2 {
		panic("please define the  manifest . Example [go test -args mymanifest.yml ]")
	}

	viper.SetConfigFile(os.Args[1])
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	man, err := manifest.New(viper.GetViper())
	if err != nil {
		panic(err)
	}
	mani = man

	h, err := NewHub(Config{Manifest: man})
	if err != nil {
		panic(err)
	}
	hub = h
}

func TestGetHub(t *testing.T) {
	tests := []struct {
		name string
		want *Hub
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefault(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetHub() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewHub(t *testing.T) {
	type args struct {
		pluginDir string
	}
	tests := []struct {
		name    string
		args    args
		want    *Hub
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewHub(tt.args.pluginDir)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewHub() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewHub() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_shortenPath(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		hub  *Hub
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.hub.shortenPath(tt.args.name); got != tt.want {
				t.Errorf("Hub.shortenPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFixDependencies(t *testing.T) {
	logrus.Info("Testing FixDependencies")
	err := FixDependencies(pluginList)
	assert.Nil(t, err)

	type args struct {
		names []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := FixDependencies(tt.args.names); (err != nil) != tt.wantErr {
				t.Errorf("FixDependencies() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGetPluginSource(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := GetPluginSource(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("GetPluginSource() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHub_GetPluginSource(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hub.GetPluginSource(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("Hub.GetPluginSource() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGetPluginBinary(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := GetPluginBinary(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("GetPluginBinary() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHub_GetPluginBinary(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hub.GetPluginBinary(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("Hub.GetPluginBinary() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOpenPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_OpenPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    interface{}
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.OpenPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.OpenPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.OpenPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenDevicePlugin(t *testing.T) {

	for _, devid := range mani.Devices() {
		for _, plugin := range mani.DeviceStack("127.0.0.1", devid) {
			dev, err := OpenDevicePlugin(plugin)
			assert.Nil(t, err)
			assert.NotNil(t, dev)
		}
	}

	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    device.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenDevicePlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenDevicePlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenDevicePlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_OpenDevicePlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    device.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.OpenDevicePlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.OpenDevicePlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.OpenDevicePlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenCoordinatorPlugin(t *testing.T) {

	logrus.Info("Testing OpenCoordinatorPlugin")
	for _, coordid := range mani.Coordinators() {
		for _, plugin := range mani.CoordinatorStack("127.0.0.1", coordid) {
			coord, err := OpenCoordinatorPlugin(plugin)
			assert.Nil(t, err)
			assert.NotNil(t, coord)
		}
	}

	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    coordinator.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenCoordinatorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenCoordinatorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenCoordinatorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_OpenCoordinatorPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    coordinator.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.OpenCoordinatorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.OpenCoordinatorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.OpenCoordinatorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenProcessorPlugin(t *testing.T) {
	logrus.Info("Testing OpenProcessorPlugin")
	for _, procid := range mani.Processors() {
		for _, plugin := range mani.ProcessorStack("127.0.0.1", procid) {
			proc, err := OpenProcessorPlugin(plugin)
			assert.Nil(t, err)
			assert.NotNil(t, proc)
		}
	}

	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    processor.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenProcessorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenProcessorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenProcessorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_OpenProcessorPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    processor.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.OpenProcessorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.OpenProcessorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.OpenProcessorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenSelectorPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    selector.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenSelectorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenSelectorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenSelectorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHub_OpenSelectorPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    selector.Plugin
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.OpenSelectorPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.OpenSelectorPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.OpenSelectorPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}
