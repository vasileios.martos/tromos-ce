// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinatormanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/coordinatormanager"

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gopkg.in/go-playground/validator.v9"
)

// Config includes the execution paramers for the CoordinatorManager
type Config struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// CoordinatorManager provides clustered access to the Coordinators
type CoordinatorManager struct {
	config Config
	closed bool
}

var validate *validator.Validate = validator.New()

// New instantiates a new CoordinatorManager
func New(config Config) (*CoordinatorManager, error) {

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Print("Cannot create new CoordinatorManager")
		return nil, err
	}

	nm := &CoordinatorManager{
		config: config,
	}

	for _, name := range config.Peer.Coordinators() {
		coord, ok := config.Peer.Coordinator(name)
		if !ok {
			panic("This should neven happen")
		}

		nm.config.Selector.Add(selector.Properties{
			ID:           coord.String(),
			Capabilities: coord.Capabilities(),
			Peer:         coord.Location(),
		})
	}
	nm.config.Selector.Commit()

	return nm, nil
}

// Close shutdowns all the coordinators of the namespace
func (nm *CoordinatorManager) Close() error {
	if nm.closed {
		return ErrClosed
	}
	nm.closed = true
	return nil
}

// Partition returns the Coordinator resposinble for the key
func (nm *CoordinatorManager) Partition(key string) (coordinator.Coordinator, error) {
	if nm.closed {
		return nil, ErrClosed
	}

	if key == "" {
		return nil, ErrInvalid
	}

	proposed, err := nm.config.Selector.Partition(key)
	if err != nil {
		return nil, err
	}

	coord, ok := nm.config.Peer.Coordinator(proposed)
	if !ok {
		return nil, ErrNotFound
	}

	return coord, nil
}
