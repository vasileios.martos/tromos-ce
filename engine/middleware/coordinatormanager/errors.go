// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinatormanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/coordinatormanager"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	// ErrCoordinatorManager is the root for the errors
	ErrCoordinatorManager = errors.NewClass("CoordinatorManager Error")

	// ErrArg describes the family of calling errors
	ErrArg = ErrCoordinatorManager.NewClass("Argument error")

	// ErrRuntime describes the family of system errors
	ErrRuntime = ErrCoordinatorManager.NewClass("Runtime error")

	// ErrInvalid is used for invalid arguments
	ErrInvalid = ErrArg.New("Invalid argument")

	// ErrNotFound indicates that the coordinator ask by key is not available
	ErrNotFound = ErrRuntime.New("Coordinator not found")

	// ErrClosed indicates the coordinator manager is closed
	ErrClosed = ErrRuntime.New("Coordinator Manager is closed")
)
