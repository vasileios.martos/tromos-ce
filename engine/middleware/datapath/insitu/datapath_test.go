// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package insitu // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"io"
	"io/ioutil"
	"sync"
	"testing"

	"code.cloudfoundry.org/bytefmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/processor"
	"go.uber.org/goleak"
	//"gitlab.com/tromos/hub/device"
	//"github.com/sirupsen/logrus"
)

var (
	MockingData []byte
)

func init() {
	filesize, err := bytefmt.ToBytes("2M")
	if err != nil {
		panic(err)
	}
	MockingData = make([]byte, filesize)
}

// Test for goroutine leaking
func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestNewDatapath(t *testing.T) {
	var err error

	_, err = NewDatapath(Config{})
	assert.NotNil(t, err) // Validator error

	path, err := NewDatapath(DefaultConfig(false))
	assert.Nil(t, err)

	err = path.Close()
	assert.Nil(t, err)

	err = path.Close()
	assert.Equal(t, err, processor.ErrClosed, "IOPATH is already closed")
}

func TestNewChannel(t *testing.T) {

	proc, err := NewDatapath(DefaultConfig(false))
	assert.Nil(t, err)

	sinks := make(map[string]string)
	// Upstream
	{
		ch, err := proc.NewChannel(processor.ChannelConfig{
			Writable: true,
			Sinks:    sinks,
		})
		assert.Nil(t, err)

		err = ch.Close()
		assert.Nil(t, err)

		err = ch.Close()
		assert.Equal(t, err, processor.ErrClosed, "IOPATH Channel is already closed")
	}

	// Downstream
	{
		ch, err := proc.NewChannel(processor.ChannelConfig{
			Writable: false,
			Sinks:    sinks,
		})
		assert.Nil(t, err)

		err = ch.Close()
		assert.Nil(t, err)

		err = ch.Close()
		assert.Equal(t, err, processor.ErrClosed, "IOPATH Channel is already closed")
	}

	err = proc.Close()
	assert.Nil(t, err)
}

func TestNewTransferWrite(t *testing.T) {
	// Direct
	newTransferWrite(t, DefaultConfig(false))

	// Multi
	newTransferWrite(t, DefaultConfig(true))
}

func newTransferWrite(t *testing.T, config Config) {
	proc, err := NewDatapath(config)
	assert.Nil(t, err)

	ch, err := proc.NewChannel(processor.ChannelConfig{Writable: true})
	assert.Nil(t, err)

	err = ch.NewTransfer(processor.Stream{})
	assert.Equal(t, err, processor.ErrInvalid, "Test empty struct")

	// Test valid stream, no data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pr,
			Port: "test",
			Meta: &processor.StreamMetadata{},
		}

		err = ch.NewTransfer(stream)
		assert.Nil(t, err)

		err = pw.Close()
		assert.Nil(t, err)
	}

	// Test valid stream, with data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pr,
			Port: "mockwriter",
			Meta: &processor.StreamMetadata{},
		}

		err = ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		n, err = pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)
	}

	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)
}

func TestNewTransferRead(t *testing.T) {
	// Direct
	newTransferRead(t, DefaultConfig(false))

	// Multi
	newTransferRead(t, DefaultConfig(true))
}

func newTransferRead(t *testing.T, config Config) {

	proc, err := NewDatapath(config)
	assert.Nil(t, err)

	sinks := make(map[string]string)
	var item processor.Stream
	// First populate some data
	{
		ch, err := proc.NewChannel(processor.ChannelConfig{
			Writable: true,
			Sinks:    sinks,
		})
		assert.Nil(t, err)

		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pr,
			Port: "mockwriter",
			Meta: &processor.StreamMetadata{},
		}
		item = stream

		err = ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)

		err = ch.Close()
		assert.Nil(t, err)
	}

	// And then retrieve them
	ch, err := proc.NewChannel(processor.ChannelConfig{
		Writable: false,
		Sinks:    sinks,
	})
	assert.Nil(t, err)

	// Test invalid stream input
	{
		err = ch.NewTransfer(processor.Stream{})
		assert.Equal(t, err, processor.ErrInvalid, "Test empty struct")

		// Conditional: Totalsize but not items
		_, pw := processor.Pipe()
		err = ch.NewTransfer(processor.Stream{
			Data: pw,
			Port: "test",
			Meta: &processor.StreamMetadata{TotalSize: len(MockingData)},
		})
		assert.Equal(t, err, processor.ErrInvalid, "Missing Items")
	}

	// Test valid stream, no data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pw,
			Port: "test",
			Meta: &processor.StreamMetadata{TotalSize: 0},
		}

		err := ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := io.Copy(ioutil.Discard, pr)
		assert.Nil(t, err)
		assert.Equal(t, n, int64(0), "0 Length read")
	}

	// Test valid stream, with data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pw,
			Port: "test",
			Meta: item.Meta,
			/*
				Meta: &processor.StreamMetadata{
					TotalSize: len(MockingData),
					State:     make(map[string]string),
					Items: map[string]device.Item{
						"RandomItem": device.Item{
							ID:   "BackendIdentifier",
							Size: uint64(len(MockingData)),
						},
					},
			*/
		}

		err := ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := io.Copy(ioutil.Discard, pr)
		assert.Nil(t, err)
		assert.Equal(t, int(n), len(MockingData), "Should read len(MockingData) bytes")

		n, err = io.Copy(ioutil.Discard, pr)
		assert.Nil(t, err)
		assert.Equal(t, int(n), 0, "Should read 0 bytes since the pipe is closed")
	}

	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)
}

func TestInterleaving(t *testing.T) {
	// Direct
	interleaving(t, DefaultConfig(false))

	// Multi
	interleaving(t, DefaultConfig(true))
}

func interleaving(t *testing.T, config Config) {
	proc, err := NewDatapath(config)
	assert.Nil(t, err)

	ch, err := proc.NewChannel(processor.ChannelConfig{Writable: true})
	assert.Nil(t, err)

	pr0, pw0 := processor.Pipe()
	stream0 := processor.Stream{
		Data: pr0,
		Port: "mockwriter0",
		Meta: &processor.StreamMetadata{},
	}
	err = ch.NewTransfer(stream0)
	assert.Nil(t, err)

	pr1, pw1 := processor.Pipe()
	stream1 := processor.Stream{
		Data: pr1,
		Port: "mockwriter1",
		Meta: &processor.StreamMetadata{},
	}
	err = ch.NewTransfer(stream1)
	assert.Nil(t, err)

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			n, err := pw0.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")
		}

		err := pw0.Close()
		assert.Nil(t, err)

		n, err := pw0.Write(MockingData)
		assert.Equal(t, err, io.ErrClosedPipe, "Pipe is closed")
		assert.Equal(t, n, 0, "Should write 0 bytes")

	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			n, err := pw1.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")
		}

		err := pw1.Close()
		assert.Nil(t, err)

		n, err := pw1.Write(MockingData)
		assert.Equal(t, err, io.ErrClosedPipe, "Pipe is closed")
		assert.Equal(t, n, 0, "Should write 0 bytes")
	}()
	wg.Wait()

	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)

}
