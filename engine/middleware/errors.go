// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	// ErrMiddleware is the root of errors caused by Middleware
	ErrMiddleware = errors.NewClass("Middleware Error")
	ErrArg        = ErrMiddleware.NewClass("Argument error")
	ErrRuntime    = ErrMiddleware.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrMiddleware.New("Backend error")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrInvalidKey = ErrArg.New("Invalid key")

	// Runtime family errors
	ErrOutOfRange = ErrRuntime.New("Out of range")
	ErrTooLarge   = ErrRuntime.New("Too large")
	ErrReadOnly   = ErrRuntime.New("File handle is read only")
	ErrWriteOnly  = ErrRuntime.New("File handle is write only")
	ErrNotADir    = ErrRuntime.New("Not a dir")
)
