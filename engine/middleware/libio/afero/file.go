// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package afero // import "gitlab.com/tromos/tromos-ce/engine/middleware/libio/afero"

import (
	"io"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/logical/augmented"
)

import (
//"gitlab.com/tromos/tromos-ce/pkg/debug"
)

const FilePathSeparator = string(filepath.Separator)

type File struct {
	// atomic requires 64-bit alignment for struct field access
	at           int64
	readDirCount int64
	closed       bool
	readOnly     bool
	writeOnly    bool
	fileData     *FileData

	// Interaction with the stable storage
	client *middleware.Client
	writer *middleware.Tx
	reader *middleware.Tx
}

func (f File) Data() *FileData {
	return f.fileData
}

// Logical contiguous space
type block struct {
	offset int64
	data   []byte
}

type FileData struct {
	sync.Mutex
	name    string
	memDir  Dir
	mode    os.FileMode
	modtime time.Time

	logger *logrus.Entry

	// Logical file representation
	logical *augmented.Tree
	cached  map[uint64]block
	dirty   []uint64
}

func (d *FileData) Name() string {
	d.Lock()
	defer d.Unlock()
	return d.name
}

func ChangeFileName(f *FileData, newname string) {
	f.Lock()
	f.name = newname
	f.Unlock()
}

func SetMode(f *FileData, mode os.FileMode) {
	f.Lock()
	f.mode = mode
	f.Unlock()
}

func SetModTime(f *FileData, mtime time.Time) {
	f.Lock()
	setModTime(f, mtime)
	f.Unlock()
}

func setModTime(f *FileData, mtime time.Time) {
	f.modtime = mtime
}

func GetFileInfo(f *FileData) *FileInfo {
	return &FileInfo{f}
}

/*
func (f *File) Open() error {
	atomic.StoreInt64(&f.at, 0)
	atomic.StoreInt64(&f.readDirCount, 0)
	f.fileData.Lock()
	f.closed = false

	if err := f.open(); err != nil {
		return err
	}

	// TODO: add cancellation context. If the operation fails the
	// file will be locked
	f.fileData.Unlock()

	f.fileData.logger.Infof("Successfully opened")
	return nil
}

func (f *File) open() error {
	return nil
}*/

func (f *File) Close() error {
	f.fileData.Lock()
	f.closed = true
	if !f.readOnly {
		setModTime(f.fileData, time.Now())
	}

	if f.writer != nil {
		if err := f.writer.Commit(); err != nil {
			return err
		}
	}

	// TODO: Release any allocated memory
	f.fileData.cached = nil
	f.fileData.dirty = nil
	f.fileData.Unlock()
	return nil
}

func (f *File) Name() string {
	return f.fileData.Name()
}

func (f *File) Stat() (os.FileInfo, error) {
	return &FileInfo{f.fileData}, nil
}

// Sync commits the current contents of the file to stable storage. Typically, this means flushing the file system's in-memory copy of recently written data to disk.
func (f *File) Sync() error {
	f.fileData.Lock()
	defer f.fileData.Unlock()
	if f.closed == true {
		return os.ErrClosed
	}

	// RDONLY does not have writers
	if f.writer != nil {
		for _, index := range f.fileData.dirty {
			page := f.fileData.cached[index]

			err := f.writer.Update(page.offset,
				func(pw io.ReadWriteCloser) (int, error) {
					wb, err := pw.Write(page.data)
					return wb, err
				})

			if err != nil {
				return err
			}
		}
	}

	// Reset dirty catalog
	f.fileData.dirty = f.fileData.dirty[:0]
	return nil
}

func (f *File) Readdir(count int) (res []os.FileInfo, err error) {
	if !f.Info().IsDir() {
		return nil, &os.PathError{Op: "readdir", Path: f.fileData.name, Err: middleware.ErrNotADir}
	}
	var outLength int64

	f.fileData.Lock()
	files := f.fileData.memDir.Files()[f.readDirCount:]
	if count > 0 {
		if len(files) < count {
			outLength = int64(len(files))
		} else {
			outLength = int64(count)
		}
		if len(files) == 0 {
			err = io.EOF
		}
	} else {
		outLength = int64(len(files))
	}
	f.readDirCount += outLength
	f.fileData.Unlock()

	res = make([]os.FileInfo, outLength)
	for i := range res {
		res[i] = &FileInfo{files[i]}
	}

	return res, err
}

func (f *File) Readdirnames(n int) (names []string, err error) {
	fi, err := f.Readdir(n)
	names = make([]string, len(fi))
	for i, f := range fi {
		_, names[i] = filepath.Split(f.Name())
	}
	return names, err
}

// Read reads up to len(b) bytes from the File. It returns the number of bytes read and any error encountered. At end of file, Read returns 0, io.EOF.
func (f *File) Read(b []byte) (n int, err error) {

	f.fileData.Lock()
	defer f.fileData.Unlock()

	if f.closed == true {
		return 0, os.ErrClosed
	}

	if f.writeOnly {
		return 0, middleware.ErrWriteOnly
	}
	datalength := f.fileData.logical.Length()

	if f.at > datalength {
		return 0, io.ErrUnexpectedEOF
	}

	if datalength-f.at >= int64(len(b)) {
		n = len(b)
	} else {
		n = int(datalength - f.at)
	}

	if n == 0 {
		return 0, io.EOF
	}
	// To avoid wasting memory by bringing all  the file data at once,
	// we only bring pages (chunks) that are later used
	// to fill the upcoming requests. Additional data are brought on demand.

	// Find the pages needed to serve the read request
	var wb int = 0
	_, segments := f.fileData.logical.Overlaps(f.at, int64(n))
	for _, segment := range segments {
		// Ensure page is in memory
		page, ok := f.fileData.cached[segment.Index]
		if !ok {
			from, data, err := f.reader.Get(segment.Index)
			if err != nil {
				return wb, err
			}

			page = block{
				offset: int64(from),
				data:   data,
			}
			f.fileData.cached[segment.Index] = page
		}

		// Fill request buffer with data from the cached page
		from := f.at + int64(wb) - segment.From
		wb += copy(b[wb:], page.data[from:])
	}

	if wb != n {
		panic("This should never happen")
	}

	// Fix the new reading offset
	atomic.AddInt64(&f.at, int64(n))
	return wb, nil
}

func (f *File) ReadAt(b []byte, off int64) (n int, err error) {
	atomic.StoreInt64(&f.at, off)
	return f.Read(b)
}

func (f *File) Truncate(size int64) error {
	if f.closed == true {
		return os.ErrClosed
	}
	if f.readOnly {
		return &os.PathError{Op: "truncate", Path: f.fileData.name, Err: middleware.ErrReadOnly}
	}
	if size < 0 {
		return middleware.ErrOutOfRange
	}
	/*
		if size > int64(len(f.fileData.data)) {
			diff := size - int64(len(f.fileData.data))
			f.fileData.data = append(f.fileData.data, bytes.Repeat([]byte{00}, int(diff))...)
		} else {
			f.fileData.data = f.fileData.data[0:size]
		}
	*/
	setModTime(f.fileData, time.Now())
	return nil
}

func (f *File) Seek(offset int64, whence int) (int64, error) {
	if f.closed == true {
		return 0, os.ErrClosed
	}
	switch whence {
	case 0:
		atomic.StoreInt64(&f.at, offset)
	case 1:
		atomic.AddInt64(&f.at, offset)
	case 2:
		//	atomic.StoreInt64(&f.at, int64(len(f.fileData.data))+offset)
	}
	return f.at, nil
}

// Write stores locally (in situ) the written data until they
// are explicitly flushed on the backend
func (f *File) Write(b []byte) (n int, err error) {
	if f.readOnly {
		return 0, &os.PathError{Op: "write", Path: f.fileData.name, Err: middleware.ErrReadOnly}
	}
	cur := atomic.LoadInt64(&f.at)
	f.fileData.Lock()
	defer f.fileData.Unlock()

	c := block{
		offset: cur,
		data:   make([]byte, len(b)),
	}
	n = copy(c.data, b)

	index := f.fileData.logical.Add(cur, int64(n))
	f.fileData.cached[index] = c
	f.fileData.dirty = append(f.fileData.dirty, index)

	setModTime(f.fileData, time.Now())

	atomic.StoreInt64(&f.at, cur+int64(n))
	return n, nil
}

func (f *File) WriteAt(b []byte, off int64) (n int, err error) {
	atomic.StoreInt64(&f.at, off)
	return f.Write(b)
}

func (f *File) WriteString(s string) (ret int, err error) {
	return f.Write([]byte(s))
}

func (f *File) Info() *FileInfo {
	return &FileInfo{f.fileData}
}

type FileInfo struct {
	*FileData
}

// Implements os.FileInfo
func (s *FileInfo) Name() string {
	s.Lock()
	_, name := filepath.Split(s.name)
	s.Unlock()
	return name
}
func (s *FileInfo) Mode() os.FileMode {
	s.Lock()
	defer s.Unlock()
	return s.mode
}
func (s *FileInfo) ModTime() time.Time {
	s.Lock()
	defer s.Unlock()
	return s.modtime
}
func (s *FileInfo) IsDir() bool {
	s.Lock()
	defer s.Unlock()
	return s.FileData.mode&os.ModeDir != 0
}
func (s *FileInfo) Sys() interface{} { return nil }
func (s *FileInfo) Size() int64 {
	if s.IsDir() {
		return int64(42)
	}
	s.Lock()
	defer s.Unlock()
	return s.FileData.logical.Length()
}
