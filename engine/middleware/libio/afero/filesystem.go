// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package afero // import "gitlab.com/tromos/tromos-ce/engine/middleware/libio/afero"

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"

	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/logical/augmented"
	"gitlab.com/tromos/tromos-ce/pkg/path"
)

// Fs is an afero.Fs implementation that abstracts Tromos as a filesystem service
// to the end-user. The filesystem maintains in memory only the cached files
type Fs struct {
	client *middleware.Client
	memDir *DirMap
}

func New(client *middleware.Client) afero.Fs {
	return &Fs{
		client: client,
		memDir: &DirMap{},
	}
}

func (s Fs) Name() string { return "tromosfs" }

// Create creates or truncates the named file. If the file already exists, it is truncated. If the file does not exist, it is created with mode 0666 (before umask). If successful, methods on the returned File can be used for I/O; the associated file descriptor has mode O_RDWR. If there is an error, it will be of type *PathError.
func (s Fs) Create(name string) (afero.File, error) {
	name = path.Normalize(name)

	if err := s.client.CreateOrReset(name); err != nil {
		return nil, err
	}

	// Create filenode and add to the hierarchy
	node := &FileData{
		name:    name,
		mode:    os.ModeTemporary,
		modtime: time.Now(),
		logger:  logrus.WithField("File", name),
	}
	s.memDir.Add(node)

	file := &File{
		client:   s.client,
		fileData: node,
	}

	logrus.Infof("Successfully created file %s", name)
	return file, nil
}

func (s Fs) Mkdir(name string, perm os.FileMode) error {

	name = path.Normalize(name)
	if err := s.client.CreateIfNotExist(name); err != nil {
		return err
	}

	// Create filenode and add to the hierarchy
	node := &FileData{
		name:   name,
		mode:   perm | os.ModeDir,
		memDir: &DirMap{},
		logger: logrus.WithField("Dir", name),
	}
	s.memDir.Add(node)

	logrus.Infof("Successfully created directory %s", name)
	//return s.client.Chmod(name, perm)
	return nil
}

func (s Fs) MkdirAll(path string, perm os.FileMode) error {
	// Fast path: if we can tell whether path is a directory or file, stop with success or error.
	dir, err := s.Stat(path)
	if err == nil {
		if dir.IsDir() {
			return nil
		}
		return err
	}

	// Slow path: make sure parent exists and then call Mkdir for path.
	i := len(path)
	for i > 0 && os.IsPathSeparator(path[i-1]) { // Skip trailing path separator.
		i--
	}

	j := i
	for j > 0 && !os.IsPathSeparator(path[j-1]) { // Scan backward over element.
		j--
	}

	if j > 1 {
		// Create parent
		err = s.MkdirAll(path[0:j-1], perm)
		if err != nil {
			return err
		}
	}

	// Parent now exists; invoke Mkdir and use its result.
	err = s.Mkdir(path, perm)
	if err != nil {
		// Handle arguments like "foo/." by
		// double-checking that directory doesn't exist.
		dir, err1 := s.Lstat(path)
		if err1 == nil && dir.IsDir() {
			return nil
		}
		return err
	}
	return nil
}

// Open opens the named file for reading. If successful, methods on the returned file can be used for reading; the
// associated file descriptor has mode O_RDONLY. If there is an error, it will be of type *PathError.
func (s Fs) Open(name string) (afero.File, error) {
	return s.open(name)
}

func (s Fs) open(name string) (*File, error) {
	rtx, err := s.client.BeginTx(name, middleware.RDONLY)
	if err != nil {
		return nil, &os.PathError{Op: "open", Path: name, Err: err}
	}

	tree := augmented.NewTree(0, 0)
	rtx.Cursor().ForEach(func(stx *middleware.SubTx) bool {
		tree.Add(int64(stx.Offset), int64(stx.Size))
		return true
	})

	// TODO: Check if what we open is a directory
	filenode := &FileData{
		name:    name,
		logical: tree,
		cached:  make(map[uint64]block),
	}

	file := &File{
		client:   s.client,
		reader:   rtx,
		fileData: filenode,
		readOnly: true,
	}
	return file, nil

}

// OpenFile is the generalized open call; most users will use Open or Create instead. It opens the named file with
// specified flag (O_RDONLY etc.). If the file does not exist, and the O_CREATE flag is passed, it is created with mode
// perm (before umask). If successful, methods on the returned File can be used for I/O. If there is an error, it will
// be of type *PathError.
func (s Fs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {

	switch {
	case flag&os.O_RDONLY != 0:
		if flag&os.O_APPEND != 0 ||
			flag&os.O_CREATE != 0 ||
			flag&os.O_EXCL != 0 ||
			flag&os.O_SYNC != 0 ||
			flag&os.O_TRUNC != 0 {
			return nil, &os.PathError{Op: "openfile", Path: name, Err: os.ErrInvalid}
		}
		return s.open(name)

	case flag&os.O_WRONLY != 0:
		if flag&os.O_TRUNC != 0 {
			if err := s.client.Truncate(name); err != nil {
				return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
			}
		}

		if flag&os.O_CREATE != 0 {
			if err := s.client.CreateIfNotExist(name); err != nil {
				return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
			}
		}

		wtx, err := s.client.BeginTx(name, middleware.WRONLY)
		if err != nil {
			return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
		}

		// TODO: Handle the rest of the flags

		node := &FileData{
			name:    name,
			mode:    perm,
			modtime: time.Now(),
			logger:  logrus.WithField("File", name),
			logical: augmented.NewTree(0, 0),
			cached:  make(map[uint64]block),
		}

		file := &File{
			client:    s.client,
			fileData:  node,
			writer:    wtx,
			writeOnly: true,
		}

		return file, nil

	case flag&os.O_RDWR != 0:
		if flag&os.O_TRUNC != 0 {
			if err := s.client.Truncate(name); err != nil {
				return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
			}
		}

		if flag&os.O_CREATE != 0 {
			if err := s.client.CreateIfNotExist(name); err != nil {
				return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
			}
		}
		// TODO: Handle the rest of the flags

		file, err := s.open(name)
		if err != nil {
			return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
		}

		wtx, err := s.client.BeginTx(name, middleware.WRONLY)
		if err != nil {
			return nil, &os.PathError{Op: "openfile", Path: name, Err: err}
		}

		file.readOnly = false
		file.writer = wtx

		return file, nil

	default:
		// Flags must be one of RDONLY, WRONLY, RDWR
		return nil, &os.PathError{Op: "openfile", Path: name, Err: os.ErrInvalid}
	}
}

func (s Fs) Remove(name string) error {
	return s.client.Remove(name)
}

func (s Fs) RemoveAll(path string) error {
	// https://github.com/golang/go/blob/master/src/os/path.go#L66
	return nil
}

func (s Fs) Rename(oldname, newname string) error {
	newname = path.Normalize(newname)
	oldname = path.Normalize(oldname)

	if newname == oldname {
		return nil
	}

	// Check that a file with the previous name exists
	old, ok := s.memDir.Find(oldname)
	if !ok {
		return afero.ErrFileNotFound
	}

	// Check that there is no other file with the new name
	_, ok = s.memDir.Find(newname)
	if ok {
		return afero.ErrDestinationExists
	}

	// Crash concerns: having a double copy is safer than having none.
	// That is why we first copy the file to its new entry and later remove
	// the old one
	s.memDir.Add(old)
	s.memDir.Remove(old)
	return nil
}

func (s Fs) Stat(name string) (os.FileInfo, error) {
	name = path.Normalize(name)

	// First try with an opened handler
	node, ok := s.memDir.Find(name)
	if ok {
		return &FileInfo{node}, nil
	}

	// Otherwise open a View (RDONLY) file and reconstruct the information
	file, err := s.Open(name)
	if err != nil {
		return nil, err
	}
	return file.Stat()
}

func (s Fs) Lstat(p string) (os.FileInfo, error) {
	panic("Operation not supported")
	//	return s.client.Lstat(p)
}

func (s Fs) Chmod(name string, mode os.FileMode) error {
	panic("Operation not supported")
	//return s.client.Chmod(name, mode)
}

func (s Fs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	panic("Operation not supported")
	//return s.client.Chtimes(name, atime, mtime)
}
