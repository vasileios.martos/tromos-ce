// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package afero // import "gitlab.com/tromos/tromos-ce/engine/middleware/libio/afero"

import (
	"os"
	"testing"
	//"io"

	"code.cloudfoundry.org/bytefmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
)

var (
	Peer     = "127.0.0.1"
	Filesize = 0
	Manifest = "./filesystem_test.yml"

	MockingData []byte
	Filesystem  afero.Fs
	mani        manifest.Manifest
)

func init() {
	viper.SetConfigFile(Manifest)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	man, err := manifest.New(viper.GetViper())
	if err != nil {
		panic(err)
	}

	mani = man

	// Stablestorage
	client, err := middleware.NewClient(middleware.Config{Manifest: man})
	if err != nil {
		panic(err)
	}

	Filesystem = New(client)

	// Dummy data
	dummysize, err := bytefmt.ToBytes("2M")
	if err != nil {
		panic(err)
	}

	Filesize = int(dummysize)
	MockingData = make([]byte, Filesize)

	// Set logger preferences
	logrus.SetLevel(logrus.DebugLevel)
}

// Open for ReadOnly
func TestOpen(t *testing.T) {
	var err error

	_, err = Filesystem.Open("")
	assert.NotNil(t, err) // Invalid Key

	_, err = Filesystem.Open(uuid.Once())
	assert.NotNil(t, err) // Key does not exist

	_, err = Filesystem.Open("testA")
	assert.NotNil(t, err) // Key does not exist (unless run not int clean state)
}

// Open for WONLY and RW
func TestOpenFile(t *testing.T) {
	var err error

	_, err = Filesystem.OpenFile("", os.O_WRONLY|os.O_CREATE, 0)
	assert.NotNil(t, err) // Invalid Key

	_, err = Filesystem.OpenFile("testA", os.O_WRONLY|os.O_CREATE, 0)
	assert.Nil(t, err)

	_, err = Filesystem.OpenFile(uuid.Once(), os.O_WRONLY|os.O_CREATE, 0)
	assert.Nil(t, err)

	_, err = Filesystem.OpenFile(uuid.Once(), os.O_WRONLY, 0)
	assert.NotNil(t, err) // Key does not exist

	_, err = Filesystem.OpenFile(uuid.Once(), os.O_CREATE, 0)
	assert.NotNil(t, err) // Invalid Argument

	file, err := Filesystem.OpenFile(uuid.Once(), os.O_CREATE|os.O_WRONLY, 0)
	assert.Nil(t, err)

	err = file.Close()
	assert.Nil(t, err)
}

func TestWrite(t *testing.T) {
	// Try to read from an WRONLY
	{
		file, err := Filesystem.OpenFile(uuid.Once(), os.O_CREATE|os.O_WRONLY, 0)
		assert.Nil(t, err)

		wb, err := file.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, wb, len(MockingData), "Written bytes less than data")

		data := make([]byte, Filesize)
		rb, err := file.Read(data)
		assert.NotNil(t, err)
		assert.Equal(t, rb, 0, "No bytes should be read")
	}

	// Try to read from an RDWR without prior sync
	{
		file, err := Filesystem.OpenFile(uuid.Once(), os.O_CREATE|os.O_RDWR, 0)
		assert.Nil(t, err)

		wb, err := file.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, wb, len(MockingData), "Written bytes less than data")

		data := make([]byte, Filesize)
		rb, err := file.Read(data)
		assert.NotNil(t, err) // EOF
		assert.Equal(t, rb, 0, "No bytes should be persisted")
	}

	// Try to read from an RDWR after sync
	{
		file, err := Filesystem.OpenFile(uuid.Once(), os.O_CREATE|os.O_RDWR, 0)
		assert.Nil(t, err)

		wb, err := file.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, wb, len(MockingData), "Written bytes less than data")

		err = file.Sync()
		assert.Nil(t, err)

		data := make([]byte, Filesize)
		rb, err := file.ReadAt(data, 0)
		assert.Nil(t, err)
		assert.Equal(t, rb, wb, "Read bytes less than written bytes")

		rb, err = file.Read(data)
		assert.NotNil(t, err) // EOF
	}

	// Try to read from an RDWR after write iteration and sync
	{
		file, err := Filesystem.OpenFile(uuid.Once(), os.O_CREATE|os.O_RDWR, 0)
		assert.Nil(t, err)

		var n int = 0
		for i := 0; i < 2; i++ {
			wb, err := file.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, wb, len(MockingData), "Written bytes less than data")

			n += wb
		}

		err = file.Sync()
		assert.Nil(t, err)

		data := make([]byte, n)
		rb, err := file.ReadAt(data, 0)
		assert.Nil(t, err)
		assert.Equal(t, rb, n, "Read bytes less than written bytes")

		rb, err = file.Read(data)
		assert.NotNil(t, err) // EOF
	}

}
