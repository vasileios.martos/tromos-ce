// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"encoding/json"
	"io"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/pkg/assertion"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
)

// Mode defines the type of the transaction
type Mode uint8

const (
	// View is the mode for retreving key information without locking
	VIEW = Mode(iota)
	// RDONLY is the mode for retreving key information with locking
	RDONLY
	// WRONLY appends changes to the log of the key
	WRONLY
)

// Return a string representation instead of a numerical
func (m Mode) String() string {
	return [...]string{"VIEW", "RDONLY", "WRONLY"}[m]
}

func (client *Client) BeginTx(key string, mode Mode) (*Tx, error) {
	if key == "" {
		return nil, ErrInvalidKey
	}

	// TODO: Find a better way to to the checking
	if !(mode == VIEW || mode == RDONLY || mode == WRONLY) {
		return nil, ErrInvalid
	}

	tid := uuid.Once()
	tx := &Tx{
		TID:    tid,
		key:    key,
		mode:   mode,
		client: client,
		logger: logrus.WithField("TX", tid),
	}

	client.logger.Print("Client.CoordinatorManager :", client.CoordinatorManager)
	partition, err := client.CoordinatorManager.Partition(key)
	if err != nil {
		return nil, err
	}

	switch mode {
	case VIEW:
		historyBins, _, err := partition.Info(key)
		if err != nil {
			return nil, err
		}

		cursor, err := populate(historyBins)
		if err != nil {
			return nil, err
		}
		tx.cursor = cursor

	case RDONLY:
		historyBins, _, err := partition.ViewStart(key, nil)
		if err != nil {
			return nil, err
		}

		cursor, err := populate(historyBins)
		if err != nil {
			// Release any locked records
			tx.Rollback()
			return nil, err
		}
		tx.cursor = cursor

	case WRONLY:

		uploader, err := client.Datapath.NewChannel(processor.ChannelConfig{
			Writable: true,
			Name:     tx.TID,
		})
		if err != nil {
			return nil, err
		}
		tx.uploader = uploader
		tx.Sinks = uploader.Sinks()

		ir, err := json.Marshal(tx.Sinks)
		if err != nil {
			return nil, err
		}

		if err := partition.UpdateStart(key, tx.ID(), ir); err != nil {
			return nil, err
		}
	}

	tx.logger.Infof("New %s TX for %s ", mode, key)
	return tx, nil
}

type SubTx struct {
	Offset int `json:"Offset"`
	Size   int `json:"Size"`

	processor.Stream
}

type Tx struct {
	// Logger specific to the transaction
	logger *logrus.Entry

	// key in the keyspace
	key string

	// Operation mode
	mode Mode

	// Protected ensures that a transaction will not usable after its closing
	closed bool

	// Pointer to cursor
	cursor *Cursor

	// Pointer to the middleware
	client *Client

	// UpdateTx
	SubTxs []*SubTx `json:"SubTxs,omitempty"`

	Sinks map[string]string `json:"Sinks,omitempty"`

	// transaction identifier
	TID string `json:"TID,omitempty"`

	// The uploader for this transaction
	uploader processor.Channel

	// Stream Identifier
	nextStream int
}

// ID returns the transaction id
func (tx *Tx) ID() string {
	return tx.TID
}

func (tx *Tx) Key() string {
	return tx.key
}

func (tx *Tx) Update(offset int64, fn func(pw io.ReadWriteCloser) (int, error)) error {
	if tx.closed {
		return os.ErrClosed
	}

	if tx.mode == RDONLY {
		return ErrReadOnly
	}

	pr, pw := processor.Pipe()

	stream := processor.Stream{
		Data: pr,
		Port: "Root",
		Meta: &processor.StreamMetadata{ID: tx.nextStream},
	}
	tx.nextStream++

	if err := tx.uploader.NewTransfer(stream); err != nil {
		return err
	}

	stx := &SubTx{
		Stream: stream,
		Offset: int(offset),
	}
	tx.SubTxs = append(tx.SubTxs, stx)

	wb, err := fn(pw)
	if err != nil {
		tx.Rollback()
		return err
	}

	if err := pw.Close(); err != nil {
		tx.Rollback()
		return err
	}

	stx.Size = wb

	return nil
}

// We want the reader to be stateless, but doing so when blocks are involved
// is tricky. Offset M may not be aligned with block begin at offset N.
// In order to navigate correctly the metadata within the block the client
// must position with M-N (alpabetically ordered)
func (tx *Tx) Get(segment uint64) (int, []byte, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	assertion.Assert(tx.mode == VIEW || tx.mode == RDONLY, "non viewable transaction")

	if tx.closed {
		panic("Reader is closed")
	}

	utx := tx.cursor.stx2Tx[segment]
	stx := tx.cursor.stxs[segment]
	data := make([]byte, stx.Size)

	pr, pw := processor.Pipe()

	// Fix the downloader to the downloader list
	downloader, ok := tx.cursor.downloaders[utx.ID()]
	if !ok {
		ch, err := tx.client.Datapath.NewChannel(processor.ChannelConfig{
			Writable: false,
			Name:     tx.TID,
			Sinks:    utx.Sinks,
		})
		if err != nil {
			return 0, nil, err
		}
		tx.cursor.downloaders[utx.ID()] = ch
		downloader = ch
	}

	stream := processor.Stream{
		Data: pw,
		Port: "root",
		Meta: &processor.StreamMetadata{
			ID:    tx.nextStream,
			State: stx.Meta.State,
			Items: stx.Meta.Items,
		},
	}
	tx.nextStream++

	if err := downloader.NewTransfer(stream); err != nil {
		return 0, nil, err
	}

	rb, err := io.ReadFull(pr, data)
	if err != nil {
		return stx.Offset, data[:rb], err
	}
	return stx.Offset, data[:rb], nil
}

// Commit writes all changes to the devices and updates the meta page on coordinators
// Returns an error if a device write error occurs, or if Commit is called on a read-only transaction.
func (tx *Tx) Commit() error {
	assertion.Assert(tx.mode == WRONLY, "Commit is called on read-only transaction")
	assertion.Assert(!tx.closed, "transaction is closed")

	// Terminate iopath
	if err := tx.uploader.Close(); err != nil {
		return err
	}

	var ur []byte
	ur, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	partition, err := tx.client.CoordinatorManager.Partition(tx.key)
	if err != nil {
		return err
	}
	if err := partition.UpdateEnd(tx.key, tx.TID, ur); err != nil {
		return err
	}

	tx.logger.Infof("Transaction successfully committed to %s", partition.String())
	return nil
}

// Rollback closes the transaction and ignores all previous updates.
// Read-only transactions must be rolled back and not committed.
func (tx *Tx) Rollback() {
	if tx.closed {
		return
	}
	tx.closed = true

	switch tx.mode {

	case VIEW:
		// Release all locked resources
		for _, d := range tx.cursor.downloaders {
			if err := d.Close(); err != nil {
				panic(err)
			}
		}

	case RDONLY:
		// Release all locked resources
		for _, d := range tx.cursor.downloaders {
			if err := d.Close(); err != nil {
				panic(err)
			}
		}

		// In this type of transaction the Coordinators keep a lease
		// on entries so to avoid being garbage collected. For the rollback,
		// we must notify the Cooordinator to release the lease
		partition, err := tx.client.CoordinatorManager.Partition(tx.Key())
		if err != nil {
			panic(err)
		}
		if err := partition.ViewEnd(tx.cursor.allocated); err != nil {
			panic(err)
		}

	case WRONLY:
		// TODO
		panic("Rollback not supported yet for writable transactions")

	}

	tx.logger.Infof("Transaction successfully rolledback")
}

func (tx *Tx) Cursor() *Cursor {
	assertion.Assert(tx.mode != WRONLY, "Commit is called on write-only transaction")
	assertion.Assert(!tx.closed, "transaction is closed")

	return tx.cursor
}

type Cursor struct {
	// List of backend metadata (for all handlers)
	stxs []*SubTx

	stx2Tx []*Tx

	// List of locked updates (not eligible for garbage collection)
	allocated []string

	// map[TID]downloader
	downloaders map[string]processor.Channel
}

// ForEach executes a function for each delta in the transaction.
// If false, stop iteration
func (c *Cursor) ForEach(fn func(stx *SubTx) bool) {
	for _, stx := range c.stxs {
		if ok := fn(stx); !ok {
			return
		}
	}
}

func populate(historyBins [][]byte) (*Cursor, error) {
	c := &Cursor{
		downloaders: make(map[string]processor.Channel),
	}

	updates := make([]*Tx, len(historyBins))
	for i, bin := range historyBins {
		if err := json.Unmarshal(bin, &updates[i]); err != nil {
			return nil, err
		}

		for j := 0; j < len(updates[i].SubTxs); j++ {
			stx := updates[i].SubTxs[j]

			if stx.Size > 0 {
				c.stxs = append(c.stxs, stx)
				c.stx2Tx = append(c.stx2Tx, updates[i])
			}
		}

		c.allocated = append(c.allocated, updates[i].ID())
	}
	return c, nil
}
