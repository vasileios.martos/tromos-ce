// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	// ErrPeer is the root of errors caused by Peer
	ErrPeer = errors.NewClass("Peer Error")

	// ErrClosed indicates that the peer is closed
	ErrClosed = ErrPeer.New("Peer is closed")

	// ErrInvalid is used when the calling arguments are wrong
	ErrInvalid = ErrPeer.New("Invalid arguments")
)
