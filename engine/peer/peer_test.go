// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"go.uber.org/goleak"
)

// Test for goroutine leaking
func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestNew(t *testing.T) {
	_, err := New(Config{})
	assert.Equal(t, err, ErrInvalid, "Invalid manifest")

	man, err := manifest.New(manifest.DefaultConfig(5, 5, 5))
	if err != nil {
		panic(err)
	}

	peer, err := New(Config{Manifest: man})
	if err != nil {
		logrus.Fatal(err)
	}

	err = peer.Close()
	assert.Nil(t, err)
}
