// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/tromos-ce/engine/proxy/device"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"gopkg.in/go-playground/validator.v9"
)

// Config defines the running paramers of the Device
type Config struct {
	ID         string       `validate:"required"`
	Parameters *viper.Viper `validate:"required"`
	Hub        *hub.Hub     `validate:"required"`
}

// Device is a virtual proxy composed out of several device.Device processing layers
type Device struct {
	toplayer     device.Device
	config       *Config
	capabilities []selector.Capability

	peer string

	logger *logrus.Entry
	closed bool
}

var _ device.Device = (*Device)(nil)
var validate *validator.Validate = validator.New()

// New returns a local proxy Device. It is used to store data
// to third-party data store
func New(config Config) (*Device, error) {

	if config == (Config{}) {
		return nil, device.ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Errorf("Cannot create device %s", config.ID)
		return nil, device.ErrInvalid
	}

	var islocal bool
	peer := config.Parameters.GetString("Proxy.host")
	if peer == "" {
		peer = "127.0.0.1"
		islocal = true
	} else {
		islocal = net.IsLocalAddress(peer)
	}

	// It is insitu when 1) there is no proxy 2) the proxy ip is a insitu ip
	var layer device.Device
	if !islocal {
		peer = config.Parameters.GetString("Proxy.host")

		proxyclient := config.Parameters.Sub("proxy")
		plugin, err := config.Hub.OpenDevicePlugin(proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}

		layer = plugin(proxyclient)
		layer.SetBackend(nil)
	} else {
		persistent := config.Parameters.Sub("persistent")
		plugin, err := config.Hub.OpenDevicePlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer = plugin(persistent)
		layer.SetBackend(nil)

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.Parameters.GetStringMap("translators"))
		for _, seqID := range stack {
			translator := config.Parameters.Sub("translators." + seqID)
			plugin, err := config.Hub.OpenDevicePlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer := plugin(translator)
			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local device exposed through proxy (the proxy ip is the insitu ip)
		if len(config.Parameters.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Parameters.Sub("proxy")
			plugin, err := config.Hub.OpenDevicePlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server := plugin(proxyserver)
			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capacity := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[capacity]
		if !ok {
			return nil, device.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	d := &Device{
		toplayer:     layer,
		config:       &config,
		capabilities: capabilities,
		peer:         peer,
		logger:       logrus.WithField("Device", config.ID),
	}

	d.logger.Infof("Found Device %s @ % s", d.String(), d.Location())
	return d, nil
}

// SetBackend defines the backend for the current device layer
func (d *Device) SetBackend(_ device.Device) {
	if d.closed {
		panic(device.ErrClosed)
	}
}

// String returns a descriptive name for the device
func (d *Device) String() string {
	if d.closed {
		panic(device.ErrClosed)
	}
	return d.config.ID
}

// Capabilities returns the capabilities of the device
func (d *Device) Capabilities() []selector.Capability {
	if d.closed {
		panic(device.ErrClosed)
	}
	return d.capabilities
}

// Location returns the node where the device is running
func (d *Device) Location() string {
	if d.closed {
		panic(device.ErrClosed)
	}
	return d.peer
}

// NewWriteChannel opens a cross-layer channel for writing data into an
// isolated collection on the backend. Collections are immutable
func (d *Device) NewWriteChannel(name string) (device.WriteChannel, error) {
	if d.closed {
		panic(device.ErrClosed)
	}
	return d.toplayer.NewWriteChannel(name)
}

// NewReadChannel opens an cross-layer channel for reading data from an
// isolated collection on the backend
func (d *Device) NewReadChannel(name string) (device.ReadChannel, error) {
	if d.closed {
		panic(device.ErrClosed)
	}

	return d.toplayer.NewReadChannel(name)
}

// Scan returns a list of items and metadata that exist on the backend
// associated with the virtual device
func (d *Device) Scan() ([]string, []device.Item, error) {
	if d.closed {
		panic(device.ErrClosed)
	}
	return d.toplayer.Scan()
}

// Close shutdowns the device
func (d *Device) Close() error {
	if d.closed {
		return device.ErrClosed
	}
	d.closed = true

	return d.toplayer.Close()
}
