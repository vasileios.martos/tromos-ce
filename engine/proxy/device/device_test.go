// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/tromos-ce/engine/proxy/device"

import (
	"io"
	"sync"
	"testing"

	"code.cloudfoundry.org/bytefmt"
	//"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"go.uber.org/goleak"
)

var (
	MockingData []byte
)

// DefaultConfig will return a Device storing data in an in-memory filesystem
func defaultConfig() Config {

	man, err := manifest.New(manifest.DefaultConfig(1, 0, 0))
	if err != nil {
		panic(err)
	}

	hub, err := hub.New(hub.Config{Path: man.Hub()})
	if err != nil {
		panic(err)
	}

	if err := hub.DownloadPlugins(man.PluginList()...); err != nil {
		panic(err)
	}

	return Config{
		ID:         "Default0",
		Parameters: man.DeviceConfiguration("Default0"),
		Hub:        hub,
	}
}

func init() {
	filesize, err := bytefmt.ToBytes("2M")
	if err != nil {
		panic(err)
	}
	MockingData = make([]byte, filesize)
}

// Test for goroutine leaking
func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestNew(t *testing.T) {
	var err error

	_, err = New(Config{})
	assert.NotNil(t, err) // Validator error

	_, err = New(Config{
		ID:         "",
		Parameters: nil,
	})
	assert.NotNil(t, err) // Validator error

	dev, err := New(defaultConfig())
	assert.Nil(t, err)

	err = dev.Close()
	assert.Nil(t, err)

	err = dev.Close()
	assert.Equal(t, err, device.ErrClosed, "Device is already closed")
}

func TestNewWriteChannel(t *testing.T) {

	dev, err := New(defaultConfig())
	assert.Nil(t, err)

	chw, err := dev.NewWriteChannel(uuid.Once())
	assert.Nil(t, err)

	for i := 0; i < 3; i++ {
		pr, pw := io.Pipe()

		err = chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})})
		assert.Nil(t, err)

		n, err := pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		n, err = pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)
	}
	err = chw.Close()
	assert.Nil(t, err)

	err = chw.Close()
	assert.Equal(t, err, device.ErrClosed, "Device Channel is already closed")

	err = dev.Close()
	assert.Nil(t, err)
}

func TestNewWriteChannelParallel(t *testing.T) {
	dev, err := New(defaultConfig())
	assert.Nil(t, err)

	chw, err := dev.NewWriteChannel(uuid.Once())
	assert.Nil(t, err)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {

		pr, pw := io.Pipe()

		err = chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})})
		assert.Nil(t, err)

		wg.Add(1)
		go func() {
			defer wg.Done()
			n, err := pw.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

			n, err = pw.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

			err = pw.Close()
			assert.Nil(t, err)
		}()
	}
	wg.Wait()

	err = chw.Close()
	assert.Nil(t, err)

	err = dev.Close()
	assert.Nil(t, err)
}

func TestNewWriteReadChannel(t *testing.T) {

	dev, err := New(defaultConfig())
	assert.Nil(t, err)

	stream := &device.Stream{Complete: make(chan struct{})}
	{
		chw, err := dev.NewWriteChannel("test")
		assert.Nil(t, err)

		pr, pw := io.Pipe()

		err = chw.NewTransfer(pr, stream)
		assert.Nil(t, err)

		n, err := pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)

		err = chw.Close()
		assert.Nil(t, err)
	}

	{
		chr, err := dev.NewReadChannel("test")
		assert.Nil(t, err)

		pr, pw := io.Pipe()

		err = chr.NewTransfer(pw, stream)
		assert.Nil(t, err)

		buf := make([]byte, len(MockingData))
		n, err := io.ReadFull(pr, buf)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should read len(MockingData) bytes")

		err = pr.Close()
		assert.Nil(t, err)

		err = chr.Close()
		assert.Nil(t, err)
	}

	err = dev.Close()
	assert.Nil(t, err)
}

/*
func TestNewWriteReadChannelMixed(t *testing.T) {

	config := DefaultConfig()
	dev, err := New(config)
	assert.Nil(t, err)

	stream := &device.Stream{Complete: make(chan struct{})}

	// Start a write
	chw, err := dev.NewWriteChannel("test")
	assert.Nil(t, err)

	{
		pr, pw := io.Pipe()

		err = chw.NewTransfer(pr, stream)
		assert.Nil(t, err)

		n, err := io.Copy(pw, bytes.NewReader(MockingData))
		assert.Nil(t, err)
		assert.Equal(t, int(n), len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)
	}

	// Interfere a read
	chr, err := dev.NewReadChannel("test")
	assert.Nil(t, err)
	{
		pr, pw := io.Pipe()

		err = chr.NewTransfer(pw, stream)
		assert.Nil(t, err)

		buf := make([]byte, 0, len(MockingData))
		n, err := io.Copy(bytes.NewBuffer(buf), pr)
		assert.Nil(t, err)
		assert.Equal(t, int(n), len(MockingData), "Should read len(MockingData) bytes")

		err = pr.Close()
		assert.Nil(t, err)
	}
	// Close both channels
	err = chw.Close()
	assert.Nil(t, err)

	err = chr.Close()
	assert.Nil(t, err)

	//err = chw.Close()
	//assert.Equal(t, err, device.ErrClosed, "Device Channel is already closed")

	err = dev.Close()
	assert.Nil(t, err)
}
*/

/*
func BenchmarkSequentialDev0(b *testing.B) {
	err := sequentialWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev1(b *testing.B) {
	err := sequentialWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}

}
func BenchmarkSequentialDev2(b *testing.B) {
	err := sequentialWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev3(b *testing.B) {
	err := sequentialWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev4(b *testing.B) {
	err := sequentialWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev5(b *testing.B) {
	err := sequentialWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev0(b *testing.B) {
	err := parallelWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev1(b *testing.B) {
	err := parallelWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}
}
func BenchmarkParallelDev2(b *testing.B) {
	err := parallelWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev3(b *testing.B) {
	err := parallelWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev4(b *testing.B) {
	err := parallelWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev5(b *testing.B) {
	err := parallelWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}
*/
